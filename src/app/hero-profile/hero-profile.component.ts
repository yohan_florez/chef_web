import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { HeroModel } from 'app/models/hero.model';
import { HttpService } from 'app/services/http.service';
import { ServicesEnum } from 'app/enums/servicesEnum';
import { UtilService } from 'app/services/util.service';

@Component({
  selector: 'app-hero-profile',
  templateUrl: './hero-profile.component.html',
  styleUrls: ['./hero-profile.component.css']
})
export class HeroProfileComponent implements OnInit {

  public hero: HeroModel;

  constructor(
    private route: ActivatedRoute,
    private httpService: HttpService,
    private readonly utilService: UtilService
  ) { }

  ngOnInit() {

    let id = this.route.snapshot.paramMap.get('id');
    
    this.httpService.getById(ServicesEnum.HERO, id).subscribe(data => {

      this.hero = data;
    });
  }

  reaction(data: any) {

    let heroLocal = localStorage.getItem('hero' + data.id + (data.like ? '' : 'non') + 'like');

    if(heroLocal) {

      let tmp = parseInt(heroLocal);
      localStorage.setItem('hero' + data.id + (data.like ? '' : 'non') + 'like', (tmp + 1).toString());
      this.utilService.showNotification('top','right', data.like ? 'success' : 'danger', (data.like ? '' : 'NO ') + 'ME GUSTA  ' + data.name);
    } else {

      localStorage.setItem('hero' + data.id + (data.like ? '' : 'non') + 'like', '1');
      this.utilService.showNotification('top','right', 'success', 'ME GUSTA  ' + data.name);
    }
  }

}
