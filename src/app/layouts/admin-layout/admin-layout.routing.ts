import { Routes } from '@angular/router';

import { HeroPresenter } from '../../hero/hero.presenter';
import { HeroProfileComponent } from '../../hero-profile/hero-profile.component';
import { RankingPresenter } from 'app/ranking/ranking.presenter';

export const AdminLayoutRoutes: Routes = [

    { path: 'superheroes',    component: HeroPresenter },
    { path: 'hero-profile/:id',   component: HeroProfileComponent },
    { path: 'ranking',    component: RankingPresenter },
];
