import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { HeroPresenter } from '../../hero/hero.presenter';
import { HeroProfileComponent } from '../../hero-profile/hero-profile.component';
import { JwPaginationComponent } from 'jw-angular-pagination';
import { SwiperComponent } from 'angular2-useful-swiper';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule
} from '@angular/material';
import { SharedModule } from 'app/components/shared.module';
import { RankingPresenter } from 'app/ranking/ranking.presenter';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    SharedModule
  ],
  declarations: [
    HeroPresenter,
    HeroProfileComponent,
    JwPaginationComponent,
    RankingPresenter,
    SwiperComponent
  ]
})

export class AdminLayoutModule {}
