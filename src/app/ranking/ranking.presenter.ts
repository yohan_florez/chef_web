import { Component, OnInit } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import {ServicesEnum} from '../enums/servicesEnum';
import { HeroModel } from 'app/models/hero.model';
import { UtilService } from 'app/services/util.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css'],
})
export class RankingPresenter implements OnInit {

  public heroList: Array<HeroModel>;

  configng = {
    autoplay: 3000, // Autoplay option having value in milliseconds
    initialSlide: 3, // Slide Index Starting from 0
    slidesPerView: 3, // Slides Visible in Single View Default is 1
    pagination: '.swiper-pagination', // Pagination Class defined
    paginationClickable: true, // Making pagination dots clicable
    nextButton: '.swiper-button-next', // Class for next button
    prevButton: '.swiper-button-prev', // Class for prev button
    spaceBetween: 5 // Space between each Item
  };

  constructor(
    private readonly httpService: HttpService,
    private readonly utilService: UtilService
  ) { }

  ngOnInit() {

    this.httpService.getAll(ServicesEnum.HERO).subscribe(data => {

      for(let h of data) {

        h.likes = localStorage.getItem('hero' + h.id + 'like') ? parseInt(localStorage.getItem('hero' + h.id + 'like')) : 0;
        h.nonlikes = localStorage.getItem('hero' + h.id + 'nonlike') ? parseInt(localStorage.getItem('hero' + h.id + 'nonlike')) : 0;
        h.total = h.likes + h.nonlikes;
      }

      data.sort((a, b) => {

        const votes1 = a.total;
        const votes2 = b.total;

        let comparison = 0;
        if (votes1 < votes2) {
          comparison = 1;
        } else if (votes1 > votes2) {
          comparison = -1;
        }
        return comparison;
      });

      this.heroList = data.filter(h => h.total > 0);;
    });
  }

  reaction(data: any) {

    let heroLocal = localStorage.getItem('hero' + data.id + (data.like ? '' : 'non') + 'like');

    if(heroLocal) {

      let tmp = parseInt(heroLocal);
      localStorage.setItem('hero' + data.id + (data.like ? '' : 'non') + 'like', (tmp + 1).toString());
      this.utilService.showNotification('top','right', data.like ? 'success' : 'danger', (data.like ? '' : 'NO ') + 'ME GUSTA  ' + data.name);
    } else {

      localStorage.setItem('hero' + data.id + (data.like ? '' : 'non') + 'like', '1');
      this.utilService.showNotification('top','right', 'success', 'ME GUSTA  ' + data.name);
    }
  }

}
