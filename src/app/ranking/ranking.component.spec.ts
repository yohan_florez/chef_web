import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroPresenter } from './hero.presenter';

describe('HeroPresenter', () => {
  let component: HeroPresenter;
  let fixture: ComponentFixture<HeroPresenter>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroPresenter ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroPresenter);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
