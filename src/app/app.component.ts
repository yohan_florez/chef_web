import { Component} from '@angular/core';
import { UserService } from './services/user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private readonly userService: UserService) {

  }

  ngOnInit() {

    this.userService.login({email: 'yflorezr@gmail.com', password: '12345678'}).then(data => {

      let response = this.userService.mapPost(data);
      localStorage.setItem('token', response.token);
    })
  }

}
