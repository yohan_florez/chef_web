import { Component, OnInit } from '@angular/core';
import { HttpService } from 'app/services/http.service';
import {ServicesEnum} from '../enums/servicesEnum';
import { HeroModel } from 'app/models/hero.model';
import { UtilService } from 'app/services/util.service';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css'],
})
export class HeroPresenter implements OnInit {

  public heroList: Array<HeroModel>;
  public  pageOfItems: Array<any>;

  constructor(
    private readonly httpService: HttpService,
    private readonly utilService: UtilService
  ) { }

  ngOnInit() {

    this.httpService.getAll(ServicesEnum.HERO).subscribe(data => {

      this.heroList = data;
    });
  }

  onChangePage(pageOfItems: Array<any>) {

    this.pageOfItems = pageOfItems;
  }

  reaction(data: any) {

    let heroLocal = localStorage.getItem('hero' + data.id + (data.like ? '' : 'non') + 'like');

    if(heroLocal) {

      let tmp = parseInt(heroLocal);
      localStorage.setItem('hero' + data.id + (data.like ? '' : 'non') + 'like', (tmp + 1).toString());
      this.utilService.showNotification('top','right', data.like ? 'success' : 'danger', (data.like ? '' : 'NO ') + 'ME GUSTA  ' + data.name);
    } else {

      localStorage.setItem('hero' + data.id + (data.like ? '' : 'non') + 'like', '1');
      this.utilService.showNotification('top','right', 'success', 'ME GUSTA  ' + data.name);
    }
  }

}
