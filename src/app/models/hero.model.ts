export class HeroModel {

    id: number;
    name: string;
    picture: string;
    publisher: string;
    info: string;
}