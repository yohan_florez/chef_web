import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HeroModel } from 'app/models/hero.model';

@Component({
  selector: 'chef-hero-card-detail',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardDetailComponent implements OnInit {

  @Input() hero : HeroModel;
  @Output() reaction = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  public action (like: boolean) {

    this.reaction.emit({id: this.hero.id, like: like, name: this.hero.name});
  }

}
