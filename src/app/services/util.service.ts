import {Injectable} from '@angular/core';
import {Headers, Response} from '@angular/http';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
declare var $: any;

@Injectable()
export class UtilService{

	@BlockUI() blockUI: NgBlockUI;

	constructor(){

	}

	blockUiStart(){
		this.blockUI.start();
	}
	
	blockUiStop(){
	    this.blockUI.stop();
	}

	getHeaderUrlencoded() {
		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		headers.append('Authorization',  'Bearer ' + localStorage.getItem('token'));
		return headers;
	}	

	getHeadersJson() {
		let headers = new Headers();
		headers.append('Accept', 'application/json');
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization',  'Bearer ' + localStorage.getItem('token'));
		return headers;
	}	

	map(response: Response): any {
		return response.json();
	}

	mapPost(response: Response): any {
		if (response['_body']) 
			return response.json();
		return response;
	}

	showNotification(from, align, type, message){
  
		$.notify({
			icon: "notifications",
			message: message
  
		},{
			type: type,
			timer: 1000,
			placement: {
				from: from,
				align: align
			},
			template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
			  '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
			  '<i class="material-icons" data-notify="icon">notifications</i> ' +
			  '<span data-notify="title">{1}</span> ' +
			  '<span data-notify="message">{2}</span>' +
			  '<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
			  '</div>' +
			  '<a href="{3}" target="{4}" data-notify="url"></a>' +
			'</div>'
		});
	}

}
