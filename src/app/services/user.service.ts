import { Injectable } from '@angular/core';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from 'app/environment';

@Injectable()
export class UserService{
	
	constructor(private http:Http){}

	login(user) {
		let options = new RequestOptions({headers: this.getHeaderLogin()});
	    return this.http.post(environment.host.api + '/login', `email=${user.email}&password=${user.password}`, options).toPromise();
	}

	private getHeaderLogin() {
		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		return headers;
	}	

	private getHeaders() {
		let headers = new Headers();
		headers.append('Accept', 'application/json');
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization',  'Bearer ' + localStorage.getItem('token'));
		return headers;
	}

	mapPost(response: Response): any {
		if (response['_body']) {
			return response.json();
		}
		return response;
	}

}

function map(response: Response): any {
	return response.json();
}