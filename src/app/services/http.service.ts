import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import { environment } from 'app/environment';

@Injectable()

export class HttpService {

  routes:any;

  constructor(private http:Http) { }

  getAll(entity) {
    const url = environment.host.api + entity;
    return this.http.get(url, { headers: this.getHeaders() }).pipe(map((response: any) => response.json()));
  }

  getById(entity, id) {
    const url = environment.host.api + entity + '/' + id;
    return this.http.get(url, { headers: this.getHeaders() }).pipe(map((response: any) => response.json()));
  }

  saveOrUpdate(entity, data, save = true) {
    const url = environment.host.api + '/' + entity;
    if (save) {
      return this.http.post( url, JSON.stringify(data), { headers: this.getHeaders() }  )
      .pipe(map((response: any) => response.json()));
    } else {
      return this.http.put( url, JSON.stringify(data), { headers: this.getHeaders() }  )
      .pipe(map((response: any) => response.json()));
    }
  }

  postRequest(entity, data) {
    const url = environment.host.api + '/' + entity;
      return this.http.post( url, JSON.stringify(data), { headers: this.getHeaders() }  )
      .pipe(map((response: any) => response.json()));
  }

  postRequest2(entity, data) {
    return new Promise((resolve, reject) => {
      const url = environment.host.api + '/' + entity;
      this.http.post( url, JSON.stringify(data), { headers: this.getHeaders() })
      .subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
    });
  }

  getRequest(entity) {
    const url = environment.host.api + '/' + entity;
    return this.http.get(url, { headers: this.getHeaders() }).pipe(map((response: any) => response.json()));
  }

  getRequestPromise(entity) {
    return new Promise((resolve, reject) => {
      const url = environment.host.api + '/' + entity;
      return this.http.get(url, { headers: this.getHeaders() })
      .subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
    })
  }

  delete(entity, id) {
    const url = environment.host.api + '/' + entity;
    return this.http.delete(url + '/' + id, { headers: this.getHeaders() }).pipe(map((response: any) => response.json()));
  }

  private getHeader() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Authorization',  'Bearer ' + localStorage.getItem('token'));
    return headers;
  }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization',  'Bearer ' + localStorage.getItem('token'));
    return headers;
  }

  readJson(): any {
		// get users from api
    return this.http.get('assets/data.json').map((response: any) => {
				return response.json()["tautos30 201810"];
			}
		)
	}

} 
